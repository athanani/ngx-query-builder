import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { QueryBuilderComponent } from './ngx-query-builder.component';
import { QueryBuilderService } from './ngx-query-builder.service';
import { QueryCompositeElementComponent } from './ngx-composite-query-element.component';
import { QueryElementComponent } from './ngx-query-element.component';
import * as locales from './i18n';
import { FormioModule } from 'angular-formio';
import { MostModule } from '@themost/angular';
import { QueryBuilderContainerComponent } from './ngx-query-builder-container.component';
@NgModule({
  declarations: [
    QueryBuilderComponent,
    QueryElementComponent,
    QueryCompositeElementComponent,
    QueryBuilderContainerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MostModule,
    TranslateModule,
    FormioModule
  ],
  exports: [
    QueryBuilderComponent,
    QueryElementComponent,
    QueryCompositeElementComponent,
    QueryBuilderContainerComponent
  ],
  entryComponents: [
    QueryBuilderContainerComponent
  ],
  providers: [
    QueryBuilderService
  ]
})
export class QueryBuilderModule {

  constructor(translateService: TranslateService) {
    Object.keys(locales).forEach((locale) => {
      if (Object.prototype.hasOwnProperty.call(locales, locale)) {
        translateService.setTranslation(locale, locales[locale], true);
      }
    });
  }
  
}
