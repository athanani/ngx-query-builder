import { animate, style, transition, trigger } from "@angular/animations";
import { AfterViewInit, Component, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { AngularDataContext } from "@themost/angular";
import { EventEmitter } from "@angular/core";
import { QueryBuilderComponent } from "./ngx-query-builder.component";
import { QueryBuilderService } from "./ngx-query-builder.service";

@Component({
    selector: 'universis-query-builder-container',
    styleUrls: [
        './ngx-query-builder.scss'
      ],
      templateUrl: './ngx-query-builder-container.component.html',
      animations: [
        trigger(
          'inOutAnimation', 
          [
            transition(
              ':enter', 
              [
                style({ opacity: 0 }),
                animate('0.5s ease-out', 
                        style({ opacity: 1 }))
              ]
            ),
            transition(
              ':leave', 
              [
                style({ opacity: 1 }),
                animate('0.25s ease-in', 
                        style({ opacity: 0 }))
              ]
            )
          ]
        )
      ],
      encapsulation: ViewEncapsulation.None
})
export class QueryBuilderContainerComponent extends QueryBuilderComponent implements OnInit, AfterViewInit, OnChanges {

    @Output() close = new EventEmitter();
    @Output() apply = new EventEmitter();

    constructor(context: AngularDataContext,
        translateService: TranslateService,
        queryBuilder: QueryBuilderService) {
            super(context, translateService, queryBuilder);
    }

    ngAfterViewInit(): void {
      super.ngAfterViewInit();
    }

    ngOnChanges(changes: SimpleChanges): void {
      super.ngOnChanges(changes);
    }

    ngOnInit(): void {
      super.ngOnInit();
    }
    

    onApply() {
        this.apply.emit(null);
    }

    onClose() {
        this.close.emit(null);
    }
}
